<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;

Route::get('/', function () {

    $obj = App::make('PdoProxy');
    //$obj->enable_slave = false;
    $stmt = $obj->query("SELECT * FROM `users` LIMIT 0,1000;");
    
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    print_r($data);
    exit;
});

Route::get('/PDO', function () {

    $obj = new PDO("mysql:host=127.0.0.1;dbname=demo","root","root");
    
    $stmt = $obj->query("show tables");
    
    $data = $stmt->fetchAll();
    
    print_r($data);
    exit;
    return view('welcome');
});

Route::get('/pdoProxy', function () {
    $obj = new pdoProxy("mysql:host=127.0.0.1;dbname=demo","root","root");
    
    $stmt = $obj->query("show tables");
    
    $data = $stmt->fetchAll();
    
    print_r($data);
    exit;
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

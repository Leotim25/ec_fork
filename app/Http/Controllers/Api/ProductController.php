<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected $product;
     
    function __construct(ProductRepository $prodcut){
        $this->product = $prodcut;
    }
    
    public function index(Request $request)
    {
        $res = $this->product->showAll();
        if($res){
            return response()->success($res);
        }
        return response()->error('product list is null');
        //
    }
}

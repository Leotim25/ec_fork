<?php

namespace App\Providers;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Connection;
use App\Database\PdoProxyConnector;
use App\Database\PdoProxyConnection;
use Illuminate\Support\ServiceProvider;

class PdoProxyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
            // Register the MySql connection class as a singleton
            // because we only want to have one, and only one,
            // MySql database connection at the same time.
            Connection::resolverFor('PdoProxy', function ($connection, $database, $prefix, $config) {
                //echo 2;
                $connection = (new PdoProxyConnector)->connect($config);
                return new PdoProxyConnection($connection, $database, $prefix, $config);
            });
            //echo 1;
            $capsule = new Capsule;
            $capsule->addConnection(config('database.connections.pdo-cp'),'pdo-cp');
            
            // 使用设置静态变量方法，令当前的 Capsule 实例全局可用
            $capsule->setAsGlobal();
            
            // 启动 Eloquent ORM
            $capsule->bootEloquent();
            
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //$this->app->make('db.connection.cp-mysql');
    }
}

# Backend Task


### API List

* get all to-do lists
* get one to-do list
* create one to-do list
* update one to-do list
* delete one to-do list
* delete all to-do list
* generate a new token
* get token status (Use tokens with TTL or RefreshToken)
 
### Notice

* Use PHPUnit test
* Use migration

### 版本
* v1.0
* v1.1 add gogogo

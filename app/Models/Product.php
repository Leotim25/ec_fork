<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    //
    use SoftDeletes;
    
    protected $fillable = ['model', 'quantity', 'amount'];
    protected $dates = ['deleted_at'];
    protected $with = array('images');
    protected $connection = 'pdo-cp';
    
    public function images()
    {
        return $this->hasMany('App\Models\ProductImage','product_id', 'id')->orderBy('sort_order');
    }
}

<?php
namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Support\Carbon;

class ProductRepository
{

    protected $product;

    protected $product_image;

    function __construct(Product $product, ProductImage $product_image)
    {
        $this->product = $product;
        $this->product_image = $product_image;
    }

    public function show($id)
    {
        $res = $this->product->where('id', $id)->get();
        if (! $res->isEmpty()) {
            return $res;
        }
        return false;
    }

    public function showAll()
    {
        $res = $this->product->get();
        if (! $res->isEmpty()) {
            return $res;
        }
        return false;
    }
}
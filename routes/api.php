<?php

use Illuminate\Support\Facades\Route;
Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
    //var_dump($query->sql);
    //var_dump($query->bindings);
    //var_dump($query->time);
});
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function ($router) {
    $router->post('login', 'Api\AuthController@login');
    $router->post('logout', 'Api\AuthController@logout');
    $router->post('refresh', 'Api\AuthController@refresh');
    Route::middleware('refresh.token')->group(function($router) {
        $router->post('profile', 'Api\AuthController@profile');
    });  
});
    Route::apiResource('product', 'Api\ProductController');
//Route::Resource('article', 'Api\ArticleController');
//Route::delete('article', 'Api\ArticleController@destroyAll');

